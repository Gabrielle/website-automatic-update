import os
import queue
import threading
from typing import List
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import fetch

website = FastAPI()
q = queue.Queue()

class WebHookProject(BaseModel):
    name: str
    default_branch: str
    id: int
    path_with_namespace: str

class WebHookCommit(BaseModel):
    id: str

class WebHookBuild(BaseModel):
    name: str
    status: str
    id: int

class WebHook(BaseModel):
    project: WebHookProject
    commit: WebHookCommit
    builds: List[WebHookBuild]

@website.get('/')
def default():
    return {'Hello':'World'}

@website.post('/hook')
def webhook(data: WebHook):
    default_branch = data.project.default_branch
    project = data.project.name
    commit_id = data.commit.id
    project_id = data.project.path_with_namespace
    job_id = None
    for build in data.builds:
        if build.status == 'success' and build.name == 'build':
            job_id = build.id
            break
    if job_id:
        q.put([project, default_branch, commit_id, project_id, job_id])
        print(default_branch, project, commit_id, project_id, job_id)
        return{'status':'ok'}
    else:
        print('No successful job named build found')
        raise HTTPException(status_code=400, detail='job unfound')

def fetching():
    website_directory = 'test_website/'
    while True:
        project, default_branch, commit_id, project_id, job_id = q.get()
        print('running fetch.py')
        fetch.fetch(project, commit_id, job_id, website_directory, fetch.base_url, project_id)
        q.task_done()

threading.Thread(target = fetching, daemon = True).start()
