# automatic-website-update

This is to automatically check and update the website when there are new succsessfull additions.
It can listen for WebHook events from GitLab CI or be triggered manually. 

## WebHook config

This program is configured based on icecast-website as a model. So the name of the job must be `build`. 
This is also the configuration that opus-website uses. 
It expects the job to upload the rendered/static website in a zipfile as an artifact.
It expects to be triggered by a pipeline event.

## Install

```
python -m venv env
source env/bin/activate
pip install uvicorn fastapi requests
uvicorn check:website --reload
```
