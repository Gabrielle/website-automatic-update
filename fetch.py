import requests
import zipfile
import os
import sys
import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('project', help = 'project website to download')
    parser.add_argument('-b','--branch', help = 'manual override branch')
    parser.add_argument('-d','--directory', help = 'set directory to unpack into')
    args = parser.parse_args()
    return args

def download(job_id, zipfile_name, base_url, project_id):
    url = f'{base_url}/{project_id}/-/jobs/{job_id}/artifacts/download'
    print(url)
    r = requests.get(url)

    with open(zipfile_name,'wb') as f:
      for chunk in r.iter_content(chunk_size=4096):
        f.write(chunk)

def fetch(project, commit_id, job_id, website_directory, base_url, project_id):
    directory_name = f'{project}-{commit_id}-{job_id}'
    zipfile_name = directory_name + '.zip'

    download(job_id, zipfile_name, base_url, project_id)

    z = zipfile.is_zipfile(zipfile_name)

    if z :
        print('unpacking into', website_directory + directory_name)

        unzip = zipfile.ZipFile(zipfile_name)
        members = unzip.infolist()

        for member in members:
            # rewrite each filename to remove elements before '/index.html' + add commit and job id
            filename_chonks = os.path.split(member.filename)
            chonks = [website_directory, directory_name] + list(filename_chonks[1:])
            member.filename = os.path.join(*chonks)
            ret = unzip.extract(member)

        print('updating symlink to downloaded version')

        os.symlink(directory_name, project)
        os.replace(project, website_directory + project)

base_url = 'https://gitlab.xiph.org'

def main():
    args = parse_args()

    project = args.project
    if args.directory:
        website_directory = f'{args.directory}/'
    else:
        website_directory = 'test_website/'
    project_id = f'xiph/{project}-website'
    project_api_id = requests.utils.quote(project_id,safe='')
    project_url = f'{base_url}/api/v4/projects/{project_api_id}'
    pipeline_url = project_url + '/pipelines'
    r = requests.get(pipeline_url)

    if r.status_code != 200:
        print('project invalid, try icecast, opus or xiph')
        sys.exit(1)
    pipelines = r.json()

    if args.branch:
        branch = args.branch
    else:
        branch = 'main'
        r= requests.get(project_url)
        if r.status_code == 200:
            project_info = r.json()
            branch = project_info['default_branch']
        else:
            print(f'warning: could not find default_branch for {project}, using {branch}')

    for pipeline in pipelines :
        if pipeline['status'] == 'success' and pipeline['ref'] == branch :

            pipeline_id = pipeline['id']

            job_url = pipeline_url + f'/{pipeline_id}/jobs'

            r = requests.get(job_url)

            jobs = r.json()

            for job in jobs:
                job_id = job['id']

                if job['status'] == 'success' :

                    commit_id = job['commit']['short_id']
                    commit_title = job['commit']['title']
                    author = job['commit']['author_name']
                    ref = job['ref']

                    print('fetching', commit_id,'on branch', ref)
                    print(author,'-', commit_title)

                    fetch(project, commit_id, job_id, website_directory, base_url, project_id)

                    break
            break

if __name__ == '__main__':
    main()
